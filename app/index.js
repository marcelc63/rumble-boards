//Dependencies
const express = require("express");
const app = express();
let http = require("http").Server(app);
let port = 3008;
const history = require("connect-history-api-fallback");
var path = require("path");

var bodyParser = require("body-parser");

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  res.header("Access-Control-Allow-Methods", "*");
  next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var mongoose = require("mongoose");
var credentials = require("./config.js");
mongoose.Promise = require("bluebird");
mongoose
  .connect(credentials.mongoConnection, {
    promiseLibrary: require("bluebird"),
    useNewUrlParser: true,
    auth: { authdb: "admin" },
    useFindAndModify: false
  })
  .then(() => console.log("connection succesful"))
  .catch(err => console.error(err));
mongoose.set("useCreateIndex", true);

var auth = require("./routes/auth");
app.use("/api/auth", auth);

var group = require("./routes/group");
app.use("/api/group", group);

var topic = require("./routes/topic");
app.use("/api/topic", topic);

var thread = require("./routes/thread");
app.use("/api/thread", thread);

var home = require("./routes/home");
app.use("/api/home", home);

var reply = require("./routes/reply");
app.use("/api/reply", reply);

var user = require("./routes/user");
app.use("/api/user", user);

app.use(history());
app.use(express.static(__dirname + "/../dist"));

app.get("*", function(req, res) {
  res.sendFile(path.join(__dirname + "/../dist/index.html"));
});

//Initiate
http.listen(port, function() {
  console.log("listening on *:", port);
});
