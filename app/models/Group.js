var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;
var URLSlugs = require("mongoose-url-slugs");

var GroupSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true
    },
    user: {
      type: ObjectId,
      ref: "User"
    },
    description: {
      type: String
    },
    date: {
      type: Date,
      default: Date.now()
    }
  },
  { toJSON: { virtuals: true } }
);

GroupSchema.plugin(URLSlugs("name", { update: true }));

GroupSchema.virtual("topics", {
  ref: "Topic",
  localField: "_id",
  foreignField: "group",
  justOne: false // set true for one-to-one relationship
});

module.exports = mongoose.model("Group", GroupSchema);
