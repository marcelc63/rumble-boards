var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var bcrypt = require("bcryptjs");
var SALT_WORK_FACTOR = 10;
var URLSlugs = require("mongoose-url-slugs");

var UserSchema = new Schema(
  {
    username: {
      type: String,
      unique: true,
      required: true
    },
    password: {
      type: String,
      required: true
    },
    usergroup: {
      type: String,
      required: true
    },
    background: {
      type: String
    },
    profpic: {
      type: String
    },
    date: {
      type: Date,
      default: Date.now()
    }
  },
  { toJSON: { virtuals: true } }
);

UserSchema.plugin(URLSlugs("username", { update: true }));

UserSchema.pre("save", function(next) {
  var user = this;
  console.log(this);
  if (this.isModified("password") || this.isNew) {
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
      if (err) {
        return next(err);
      }
      console.log("salt", salt);
      bcrypt.hash(user.password, salt, function(err, hash) {
        if (err) {
          return next(err);
        }
        console.log(hash);
        user.password = hash;
        console.log(user.password);
        next();
      });
    });
  } else {
    return next();
  }
});

UserSchema.methods.comparePassword = function(passw, cb) {
  console.log("compare", passw, this);
  bcrypt.compare(passw, this.password, function(err, isMatch) {
    if (err) {
      return cb(err);
    }
    cb(null, isMatch);
  });
};

UserSchema.virtual("postsCount", {
  ref: "Post",
  localField: "_id",
  foreignField: "user",
  count: true,
  justOne: false // set true for one-to-one relationship
});

module.exports = mongoose.model("User", UserSchema);
