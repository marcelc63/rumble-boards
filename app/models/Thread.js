var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;
var URLSlugs = require("mongoose-url-slugs");

var ThreadSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    content: {
      type: String
    },
    topic: {
      type: ObjectId,
      ref: "Topic"
    },
    user: {
      type: ObjectId,
      ref: "User"
    },
    date: {
      type: Date,
      default: Date.now()
    }
  },
  { toJSON: { virtuals: true } }
);

ThreadSchema.plugin(URLSlugs("name", { update: true }));

ThreadSchema.virtual("posts", {
  ref: "Post",
  localField: "_id",
  foreignField: "thread",
  justOne: false // set true for one-to-one relationship
});

ThreadSchema.virtual("postsCount", {
  ref: "Post",
  localField: "_id",
  foreignField: "thread",
  count: true,
  justOne: false // set true for one-to-one relationship
});

module.exports = mongoose.model("Thread", ThreadSchema);
