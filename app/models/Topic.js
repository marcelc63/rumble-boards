var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;
var URLSlugs = require("mongoose-url-slugs");

var TopicSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true
    },
    group: {
      type: ObjectId,
      ref: "Group"
    },
    user: {
      type: ObjectId,
      ref: "User"
    },
    description: {
      type: String
    },
    date: {
      type: Date,
      default: Date.now()
    }
  },
  { toJSON: { virtuals: true } }
);

TopicSchema.plugin(URLSlugs("name", { update: true }));

TopicSchema.virtual("threads", {
  ref: "Thread",
  localField: "_id",
  foreignField: "topic",
  justOne: false // set true for one-to-one relationship
});

TopicSchema.virtual("threadsCount", {
  ref: "Thread",
  localField: "_id",
  foreignField: "topic",
  count: true,
  justOne: false // set true for one-to-one relationship
});

TopicSchema.virtual("postsCount", {
  ref: "Post",
  localField: "_id",
  foreignField: "topic",
  count: true,
  justOne: false // set true for one-to-one relationship
});

module.exports = mongoose.model("Topic", TopicSchema);
