var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;
var URLSlugs = require("mongoose-url-slugs");

var PostSchema = new Schema({
  content: {
    type: String,
    required: true
  },
  topic: {
    type: ObjectId,
    ref: "Thread"
  },
  thread: {
    type: ObjectId,
    ref: "Thread"
  },
  post: {
    type: ObjectId,
    ref: "Post"
  },
  user: {
    type: ObjectId,
    ref: "User"
  },
  date: {
    type: Date,
    default: Date.now()
  }
});

module.exports = mongoose.model("Post", PostSchema);
