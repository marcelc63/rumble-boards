var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Group = require("../models/Group.js");
var Topic = require("../models/Topic.js");
var Thread = require("../models/Thread.js");
var passport = require("passport");
require("../config/passport")(passport);

function getToken(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(" ");
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
}

/* GET ONE */
router.get("/", function(req, res) {
  Group.find()
    .populate({
      path: "topics",
      populate: [
        { path: "threadsCount" },
        { path: "postsCount" },
        {
          path: "threads",
          options: {
            limit: 1,
            sort: { created: -1 },
            populate: {
              path: "posts",
              options: { limit: 1, sort: { created: -1 } },
              populate: { path: "user" }
            }
          }
        }
      ]
    })
    .exec({}, function(err, docs) {
      if (err) return next(err);
      res.json(docs);
    });
});

router.get("/topic/:slug", function(req, res) {
  console.log(req.params.slug);
  Topic.findOne({ slug: req.params.slug })
    .populate({
      path: "threads",
      populate: [
        { path: "postsCount" },
        { path: "user" },
        {
          path: "posts",
          options: {
            limit: 1,
            sort: { created: -1 }
          },
          populate: { path: "user" }
        }
      ]
    })
    .populate("user")
    .exec({}, function(err, docs) {
      if (err) return next(err);
      console.log("HII");
      console.log(docs);
      res.json(docs);
    });
});

router.get("/thread/:slug", function(req, res) {
  console.log(req.params.slug);
  Thread.findOne({ slug: req.params.slug })
    .populate({ path: "user", populate: { path: "postsCount" } })
    .populate("topic")
    .populate([
      {
        path: "posts",
        populate: { path: "user", populate: { path: "postsCount" } }
      }
    ])
    .exec({}, function(err, docs) {
      if (err) return next(err);
      console.log("HII");
      console.log(docs);
      res.json(docs);
    });
});

module.exports = router;
