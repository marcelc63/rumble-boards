var express = require("express");
var router = express.Router();
var mongoose = require("mongoose");
var Model = require("../models/User.js");
var passport = require("passport");
require("../config/passport")(passport);

function getToken(headers) {
  if (headers && headers.authorization) {
    var parted = headers.authorization.split(" ");
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
}

/* GET ONE */
router.get("/", passport.authenticate("jwt", { session: false }), function(
  req,
  res
) {
  var token = getToken(req.headers);
  if (token) {
    Model.findOne({ _id: req.user._id }, function(err, docs) {
      if (err) return next(err);
      res.json(docs);
    });
  }
});

/* UPDATE */
router.post("/", passport.authenticate("jwt", { session: false }), function(
  req,
  res,
  next
) {
  // console.log("update");
  var token = getToken(req.headers);
  if (token) {
    let payload = {
      background: req.body.background,
      profpic: req.body.profpic
    };
    Model.findOneAndUpdate({ _id: req.user._id }, payload, function(err, post) {
      if (err) return next(err);
      res.json(post);
    });
  } else {
    return res.status(403).send({ success: false, msg: "Unauthorized." });
  }
});

/* DELETE */
router.post(
  "/delete/:id",
  passport.authenticate("jwt", { session: false }),
  function(req, res) {
    // console.log("delete");
    var token = getToken(req.headers);
    if (token) {
      Model.findByIdAndRemove(req.params.id, function(err, post) {
        if (err) return console.log(err);
        console.log("done");
        res.json(post);
      });
    } else {
      return res.status(403).send({ success: false, msg: "Unauthorized." });
    }
  }
);

module.exports = router;
