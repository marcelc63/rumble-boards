import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loggedIn: false,
    background: false,
    topic: "",
    thread: ""
  },
  mutations: {
    change(state, payload) {
      let { key, val } = payload;
      state[key] = val;
    }
  },
  actions: {
    change({ commit }, payload) {
      commit("change", payload);
    }
  },
  getters: {
    loggedIn(state) {
      return state.loggedIn;
    },
    background(state) {
      return state.background;
    },
    topic(state) {
      return state.topic;
    },
    thread(state) {
      return state.thread;
    }
  }
});
